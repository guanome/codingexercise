﻿using Skillz.Utils;
using System.Web.Mvc;

namespace Skillz.Controllers
{
    public class BaseController : Controller 
    {
        protected JavascriptViewResult JavascriptView(string viewName)
        {
            return JavascriptView(viewName, null);
        }

        protected JavascriptViewResult ReplaceHtml(string selector, string viewName, object model,
            bool showAlerts = true)
        {
            var replaceHtmlViewModel = new ReplaceHtmlViewModel
            {
                Model = model,
                ShowAlerts = showAlerts,
                Selector = selector,
                ViewName = viewName
            };
            return JavascriptView("_ReplaceHtml.js", replaceHtmlViewModel);
        }

        protected JavascriptViewResult JavascriptView(string viewName, object model)
        {
            if (model != null)
                ViewData.Model = model;
            var javascriptViewResult = new JavascriptViewResult
            {
                ViewName = viewName,
                ViewData = ViewData,
                TempData = TempData,
                ViewEngineCollection = ViewEngineCollection
            };
            return javascriptViewResult;
        }
    }
}