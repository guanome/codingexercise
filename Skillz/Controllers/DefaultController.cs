﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Skillz.Controllers
{
    public class DefaultController : BaseController
    {
        private Models.CarContext db;
        public DefaultController(Models.CarContext dbContext)
        {            
            this.db = dbContext;
        }

        public DefaultController()
        {
            this.db = new Models.CarContext();
        }

        [HttpGet]
        public ActionResult Index(bool showDeleted = false)
        {
            ViewBag.ShowDeleted = showDeleted;
            using (db)
            {
                IQueryable<Models.Car> cars = showDeleted ? db.Cars : db.Cars.Where(x=> !x.IsDeleted);
                var sortedCars = cars.OrderBy(x => x.IsDeleted).ToList();
                if (Request.IsAjaxRequest())
                    return ReplaceHtml("#carsList", "_CarsList", sortedCars);
                return View(sortedCars);
            }
        }

        public ActionResult Create()
        {
            return View("Edit");
        }

        [HttpPost]
        public ActionResult Create(Models.Car frank)
        {
            if (!ModelState.IsValid) return View("Edit", frank);
            using (db)
            {
                db.Cars.Add(frank);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            using (db)
            {
                var car = db.Cars.SingleOrDefault(x => x.Id == id);
                if (car == null)
                    return RedirectToAction("Index");
                return View(car);
            }
        }
        [HttpPost]
        public ActionResult Edit(int id, Models.Car frank)
        {
            if (!ModelState.IsValid) return View("Edit", frank);
            using (db)
            {
                db.Cars.AddOrUpdate(frank);
                db.SaveChanges();
            }
            return RedirectToAction("Edit", new { id });
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            using (db)
            {
                var car = db.Cars.Single(x => x.Id == id);
                car.IsDeleted = true;
                db.SaveChanges();
            }
            return Content("OK");// RedirectToAction("Index");
        }
    }
}