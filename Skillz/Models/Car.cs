﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Skillz.Models
{
    public class Car
    {
        [Key, Column("Id")]
        public int Id { get; set; }
        [Required]
        public string Make { get; set; }
        [Required]
        public string Model { get; set; }
        [Required]
        public string Color { get; set; }
        [Required]
        public int Price { get; set; }
        [Required, Display(Name = "Trim Level")]
        public string TrimLevel { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class CarContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
    }
}