﻿using System.Web.Mvc;

namespace Skillz.Utils
{
    public class JavascriptResultEx : PartialViewResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "text/javascript";
            base.ExecuteResult(context);
        }
    }
}