﻿using System.Web.Mvc;

namespace Skillz.Utils
{
    public class JavascriptViewResult : PartialViewResult
    {
        public JavascriptViewResult()
        {

        }

        public JavascriptViewResult(object model)
        {
            ViewData.Model = model;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = "application/javascript";
            base.ExecuteResult(context);
        }
    }
}