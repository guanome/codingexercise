﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Skillz.Utils
{
    public class ReplaceHtmlViewModel
    {
        public object Model { get; set; }
        public string Selector { get; set; }
        public string ViewName { get; set; }
        public bool ShowAlerts { get; set; }
    }
}