namespace Skillz.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Skillz.Models.CarContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Skillz.Models.CarContext";
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Skillz.Models.CarContext context)
        {
            context.Database.ExecuteSqlCommand("TRUNCATE table Cars");
            context.Cars.Add(new Models.Car()
            {
                Color = "Gray",
                Make = "Toyota",
                Model = "Tercel",
                Price = 500,
                TrimLevel = "Base"
            });
            context.Cars.Add(new Models.Car()
            {
                Color = "Red",
                Make = "Nissan",
                Model = "Sentra",
                Price = 1200,
                TrimLevel = "Base"
            });
            context.Cars.Add(new Models.Car()
            {
                Color = "White",
                Make = "Mazda",
                Model = "B2300",
                Price = 0,
                TrimLevel = "Base"
            });
            context.Cars.Add(new Models.Car()
            {
                Color = "Champagne",
                Make = "Toyota",
                Model = "Avalon",
                Price = 0,
                TrimLevel = "XLS"
            });
            context.Cars.Add(new Models.Car()
            {
                Color = "Forest Green",
                Make = "Subaru",
                Model = "Outback",
                Price = 4000,
                TrimLevel = "2.5i"
            });
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
